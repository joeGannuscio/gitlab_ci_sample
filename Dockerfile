FROM python:latest
RUN apk update && apk add python3-dev

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN pip install -r requirements.txt

RUN mkdir /gitlab_ci_sample
WORKDIR /gitlab_ci_sample
COPY ./gitlab_ci_sample /gitlab_ci_sample

RUN adduser -D user
USER user

cmd python manage.py runserver 0.0.0.0:$PORT